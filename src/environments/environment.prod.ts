export const environment = {
  firebaseConfig: {
    apiKey: 'AIzaSyC_LuDhV_RMG5_kOI2tebC0_CGzU9jAL40',

    authDomain: 'changitoapp.firebaseapp.com',

    projectId: 'changitoapp',

    storageBucket: 'changitoapp.appspot.com',

    messagingSenderId: '18934570819',

    appId: '1:18934570819:web:6297ba2fcb23549cfed9f6',
  },

  production: true,
  actionCodeSettings: {
    url: 'http://localhost:4200/demo',
    handleCodeInApp: true,
  },
};
