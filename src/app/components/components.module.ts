import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotifierComponent } from './notifier/notifier.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [NotifierComponent],
  imports: [CommonModule, MaterialModule],
})
export class ComponentsModule {}
