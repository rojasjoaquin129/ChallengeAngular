import { Injectable } from '@angular/core';
import { Products } from '../models/backend/products';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  constructor() {}

  /**
   * este array de objeto tiene la funcion de hacer funcionar  el componente ad-grid
   * por mi falta de comprencion del ad-grid
   */

  public element: Products[] = [
    {
      description:
        'Auriculares JBL Tune 500 negro. Modo manos libres incluido. Asistente de voz integrados: Google Assistant y Siri. Con micrófono incorporado. El largo del cable es de 120 cm. Sonido superior y sin límites. Cómodos y prácticos.',
      id: '1Dno5itoyZcONkIaqRYv',
      img: 'https://http2.mlstatic.com/D_NQ_NP_2X_799004-MLA46345632217_062021-F.webp',
      make: 'JBL',
      name: 'Auriculares',
      price: 3300,
    },
    {
      description:
        'Lavarropas automático inverter. Carga frontal de 9 kg. Incluye 11 programas. Centrifuga a una velocidad de 1400 rpm. Apto para lavar con agua a diversas temperaturas. Tambor de acero inoxidable y filtro atrapa pelusas. Eficiencia energética A++. Comodidad y practicidad en tu hogar.',
      id: '2e6UiCZQlWeVLuQX7OPn',
      img: 'https://http2.mlstatic.com/D_NQ_NP_808150-MLA32541827541_102019-O.webp',
      make: 'Samsung',
      name: 'Lavarropas',
      price: 98000,
    },
    {
      description:
        'Heladera Columbia blanca con freezer. Capacidad de 317 litros. Eficiencia energética A+. Posee puerta reversible. Con iluminación interior. Cuenta con porta huevos.',
      id: '5FCX1u6gOMbE0VEjyBvr',
      img: 'https://http2.mlstatic.com/D_NQ_NP_2X_985832-MLA45257574369_032021-F.webp',
      make: 'Columbia',
      name: 'Heladera',
      price: 73500,
    },
    {
      description:
        'Notebook Enova Intel Core I5. Posee una memoria RAM de 8 GB. Pantalla de 14 pulgadas.  ',
      id: '8fstDa0L78UVmNHb60JC',
      img: 'https://http2.mlstatic.com/D_NQ_NP_2X_858173-MLA47208339201_082021-F.webp',
      make: 'Enova',
      name: 'Notebook',
      price: 120000,
    },
    {
      description:
        'Microondas Grill Atma blanco. Potencia de 800 w. Potencia de grill de 1000w. Display digital. Cuenta con bloqueo de seguridad. Su eficiencia energética es B. Con descongelado automático por peso. Encontrá en sus 8 programas la función indicada para tus comidas.',
      id: 'A1Q3GuoQxQeFbRTTivEx',
      img: 'https://http2.mlstatic.com/D_NQ_NP_771597-MLA41331177489_042020-O.webp',
      make: 'Atma',
      name: 'Microondas',
      price: 22000,
    },
    {
      description:
        ' Barra de Sonido infinity 8. Con potencia 150w, cuenta con entrada USB y bluetooth. Incluye control remoto',
      id: 'ElvUtzCC4iRJxRrwkxh1',
      img: 'https://http2.mlstatic.com/D_NQ_NP_2X_958063-MLA44133357718_112020-F.webp',
      make: 'Novik',
      name: 'Barra de Sonido',
      price: 16000,
    },
    {
      description:
        'Aire acondicionado Sanyo split inverter. Capacidad de refrigeración de 3300 W. Capacidad de calefacción de 3250 W. Cuenta con función de dormir. Incluye control remoto. Eficiencia energética A.',
      id: 'STgrl5LAhvl5GZS6jxmZ',
      img: 'https://http2.mlstatic.com/D_NQ_NP_677202-MLA40176282059_122019-O.webp',
      make: 'Sanyo',
      name: 'Aire acondicionado',
      price: 64500,
    },
    {
      description:
        'Smart TV AOC LED 32". Su resolución es HD. Con Web browser, Netflix, Youtube. Conectá tus dispositivos mediante sus 3 puertos HDMI y sus 2 puertos USB. Entretenimiento y conectividad en tu hogar.',
      id: 'WCUR5snbHV87hKJA2TFf',
      img: 'https://http2.mlstatic.com/D_NQ_NP_2X_849781-MLA41969993540_052020-F.webp',
      make: 'AOC',
      name: 'SmartTV',
      price: 27999,
    },
    {
      description:
        'Bicicleta Raleigh Mojave 2.0 dama. Rodado 29. Transmision 21 velocidades shimano tourney. Plato y palanca shimano. Caja pedalera shimano. Caja shimano. Caño para asiento de aluminio. Horquilla con suspensión. Frenos a discos mecánicos ',
      id: 'q5tXeUdmbcbItBSfQqXd',
      img: 'https://http2.mlstatic.com/D_NQ_NP_797049-MLA32820281587_112019-O.webp',
      make: 'Raleigh Mojave',
      name: 'Bicicleta',
      price: 66800,
    },
    {
      description:
        'Xiaomi Mi 11 Lite. Compatible con redes 5G. Pantalla AMOLED de 6.55". Posee 3 cámaras traseras de 64, 8 y 5 Mpx. Cámara delantera de 20 Mpx. Con 8 GB de RAM. Con reconocimiento facil y sensor de huella dactilar.',
      id: 'rqAur5HX77n0YfmLJ8yb',
      img: 'https://http2.mlstatic.com/D_NQ_NP_2X_797325-MLA48495988639_122021-F.webp',
      make: 'Xiaomi',
      name: 'Celular ',
      price: 81000,
    },
  ];
}
