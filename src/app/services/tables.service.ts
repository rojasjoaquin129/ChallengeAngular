import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { Carts } from '../models/backend/carts';
import { Products } from '../models/backend/products';
import { ProductsCarts } from '../models/backend/product_carts';
import { Orden } from '../models/frontend/orden';
import { proCart } from '../models/frontend/proCart';

@Injectable({
  providedIn: 'root',
})
export class TablesService {
  productsRef: AngularFirestoreCollection<Products>;
  cartRef: AngularFirestoreCollection<Carts>;
  producCartsRef: AngularFirestoreCollection<ProductsCarts>;
  OrdenRef: AngularFirestoreCollection<Orden>;
  constructor(private afs: AngularFirestore) {
    this.producCartsRef = this.afs.collection('product_carts');
    this.cartRef = this.afs.collection('carts');
    this.productsRef = this.afs.collection('products');
    this.OrdenRef = this.afs.collection('orden');
  }
  // -------GET ALL
  getAllProductCart() {
    return this.producCartsRef.valueChanges() as Observable<ProductsCarts[]>;
  }
  getAllcarts() {
    return this.cartRef.valueChanges() as Observable<Carts[]>;
  }
  getAllProducts() {
    return this.productsRef.valueChanges() as Observable<Products[]>;
  }

  // -----------------------CARTS
  createCart(id: string) {
    let objet: Carts = { status: 'pending', id: id };
    return this.cartRef.doc(id).set(objet);
  }
  updateCart(id: string) {
    let objet = { status: 'completed', id: id };
    return this.afs.collection('carts').doc(id).update(objet);
  }
  //--------------------CREATE
  createOrden(cant: proCart[]) {
    const orden: Orden = {
      collection: cant,
    };
    return this.OrdenRef.add(orden);
  }
  createProductCart(idProduct: string, cant: number) {
    const idP = this.afs.createId();
    const id = this.afs.createId();

    this.createCart(id);
    const ProCart: ProductsCarts = {
      cart_id: id,
      product_id: idProduct,
      quantity: cant,
      id: idP,
    };
    console.log(ProCart);
    return this.producCartsRef.doc(idP).set(ProCart);
  }
  //------------------------UPDAte
  updataProductCart(id: string, product: ProductsCarts) {
    return this.producCartsRef.doc(id).update(product);
  }

  //--------------------DELATE
  delateProductCart(id: string) {
    return this.producCartsRef.doc(id).delete();
  }
  delateCart(id: string) {
    return this.cartRef.doc(id).delete();
  }
  // -----------get ONE
  getOneProduct(id: string) {
    return this.productsRef.doc(id).valueChanges() as Observable<Products>;
  }
  getOneCart(id: string) {
    return this.cartRef.doc(id).valueChanges() as Observable<Carts>;
  }
  getOneProCart(id: string) {
    return this.producCartsRef
      .doc(id)
      .valueChanges() as Observable<ProductsCarts>;
  }
}
