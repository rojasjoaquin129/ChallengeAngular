export interface Products {
  id: string;
  name: string;
  price: number;
  description: string;
  img: string;
  make: string;
}
