export interface ProductsCarts {
  product_id: string;
  cart_id: string;
  quantity: number;
  id: string;
}
