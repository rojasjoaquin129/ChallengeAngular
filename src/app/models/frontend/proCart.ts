import { Products } from '../backend/products';

export interface proCart {
  product: Products;
  cantidad: number;
  id_productCart: string;
  id_cart: string;
  product_id: string;
}
