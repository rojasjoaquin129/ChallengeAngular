import { Component, OnInit, resolveForwardRef } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { Auth } from 'src/app/models/frontend/auth.ts/auth';
import { AuthService } from 'src/app/services/auth.service';
import { NotificationService } from 'src/app/services/notification.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  constructor(
    private service: AuthService,
    private alert: NotificationService,
    private router: Router
  ) {}

  ngOnInit(): void {}
  form = new FormGroup({});
  model: Auth = {};

  fields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'input',
      templateOptions: {
        label: 'Email',
        placeholder: 'Input placeholder',
        required: true,
      },
    },
    {
      key: 'password',
      type: 'input',
      templateOptions: {
        label: 'password',
        placeholder: 'ingrese su contraseña',
        type: 'password',
        required: true,
        minLength: 6,
      },
      validation: [Validators.required, Validators.minLength(6)],
    },
  ];

  onSubmit() {
    if (this.form.valid) {
      if (this.model.email && this.model.password) {
        this.service
          .register(this.model.email, this.model.password)
          .then((result) => {
            console.log(result);
            result?.user?.sendEmailVerification().then(() => {
              this.alert.toastSuccess(
                'Se envio el mensaje para poder acceder '
              );
              this.router.navigate(['login']);
            });
          })
          .catch((error) => {
            this.alert.mensajeError('Problemas con la contraseña o el correo');
          });
      }
    } else {
      this.alert.mensajeError('NO Se valido el formulario');
      this.model.email = '';
      this.model.password = '';
    }
  }
}
