import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngrx/store';
import { AuthService } from 'src/app/services/auth.service';
import { AppState } from 'src/app/store';
import * as selectiones from 'src/app/store/selector/auth.selector';
import * as actiones from 'src/app/store/actions/auth.actions';

import { NotificationService } from 'src/app/services/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  hide = true;
  constructor(
    private fb: FormBuilder,
    private service: AuthService,
    private alert: NotificationService,
    private router: Router,
    private store: Store<AppState>
  ) {
    this.form = this.fb.group({
      user: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  login() {
    if (this.form.invalid) {
      this.form.reset();
    } else {
      const { user, password } = this.form.value;

      // this.store.dispatch(actiones.SignIn({ email: user, password: password }));
      this.service
        .login(user, password)
        .then((result) => {
          console.log(result.user?.emailVerified);
          if (!result.user?.emailVerified) {
            this.alert.mensajeError('Activar Email para poder acceder');
          } else {
            this.router.navigate(['/dashboard']);
          }
        })
        .catch((erro) => {
          this.alert.mensajeError('error en la contraseña o/email');
          this.form.reset;
        });
    }
  }

  isValidField(field: string) {
    return (
      (this.form.get(field)?.touched || this.form.get(field)?.dirty) &&
      !this.form.get(field)?.valid
    );
  }

  getErrorMessage(field: string) {
    let message;
    if (this.form.get(field)?.errors?.['required']) {
      message = 'El campo ' + field + ' no puede estar vacio';
    } else if (this.form.get(field)?.hasError('email')) {
      message = 'No es un formato valido de email ';
    } else if (this.form.get(field)?.hasError('minlength')) {
      const minLength =
        this.form.get(field)?.errors?.['minlength'].requieredLength;
      message = 'La contraseña tiene q tener mas de 6 caracteres';
    }
    return message;
  }
  ngOnInit(): void {}
}
