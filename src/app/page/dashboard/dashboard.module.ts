import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CarritoComponent } from './carrito/carrito.component';
import { ListProductComponent } from './list-product/list-product.component';
import { MaterialModule } from 'src/app/material/material.module';
import { AgGridModule } from 'ag-grid-angular';
import { ComponentsModule } from './components/components.module';
import { DialogComponent } from './components/dialog/dialog.component';

@NgModule({
  declarations: [DashboardComponent, CarritoComponent, ListProductComponent],
  entryComponents: [DialogComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MaterialModule,
    ComponentsModule,
    AgGridModule.withComponents([]),
  ],
})
export class DashboardModule {}
