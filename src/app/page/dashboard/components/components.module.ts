import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { MaterialModule } from 'src/app/material/material.module';
import { AgGridComponent } from './ag-grid/ag-grid.component';
import { AgGridModule } from 'ag-grid-angular';
import { RouterModule } from '@angular/router';
import { DialogComponent } from './dialog/dialog.component';

@NgModule({
  declarations: [NavbarComponent, AgGridComponent, DialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    AgGridModule.withComponents([]),
    RouterModule,
  ],
  exports: [NavbarComponent, AgGridComponent, DialogComponent],
})
export class ComponentsModule {}
