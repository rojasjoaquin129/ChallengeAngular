import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { ColDef } from 'ag-grid-community';
import { ProductsService } from 'src/app/services/products.service';
import { TablesService } from 'src/app/services/tables.service';

@Component({
  selector: 'app-ag-grid',
  templateUrl: './ag-grid.component.html',
  styleUrls: ['./ag-grid.component.scss'],
})
export class AgGridComponent implements OnInit {
  @Output() product: EventEmitter<any>;
  @ViewChild('agGrid') agGrid!: AgGridAngular;
  defaultColDef: any;

  columnDefs: ColDef[] = [
    {
      field: 'name',
      sortable: true,
      filter: true,
      flex: 1,
      cellClass: 'grid-cell-centered',
      headerClass: 'ag-header-cell-label',
    },
    {
      field: 'make',
      sortable: true,
      filter: true,
      flex: 1,
      cellClass: 'grid-cell-centered',
      headerClass: 'ag-header-cell-label',
    },
    {
      field: 'price',
      sortable: true,
      filter: true,
      flex: 1,
      cellClass: 'grid-cell-centered number-cell',
      headerClass: 'ag-header-cell-label',
    },
  ];

  rowData = this.service.element;

  onSelectionChanged(event: any) {
    const selectedRows = this.agGrid.api.getSelectedRows();
    this.product.emit(selectedRows[0]);
  }
  constructor(private service: ProductsService) {
    this.product = new EventEmitter<any>();
    this.defaultColDef = {
      resizable: true,
    };
  }

  ngOnInit(): void {}
}
