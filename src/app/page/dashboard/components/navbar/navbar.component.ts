import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit, OnDestroy {
  hidden = false;
  cant: number = 0;
  constructor(
    private dataService: DataService,
    private authS: AuthService,
    private router: Router
  ) {}
  public user: any;
  public user$: Observable<any> = this.authS.afAuth.user;
  shopping() {
    this.hidden = true;
    this.cant = 0;
  }
  ngOnInit(): void {
    this.dataService.increm$.subscribe((numer) => {
      this.cant++;
    });
  }
  ngOnDestroy() {}

  logout() {
    this.authS.logOut().then(() => this.router.navigate(['/login']));
  }
}
