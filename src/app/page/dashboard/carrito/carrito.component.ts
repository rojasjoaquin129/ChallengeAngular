import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { timer } from 'rxjs';
import { ProductsCarts } from 'src/app/models/backend/product_carts';
import Swal from 'sweetalert2';
import { proCart } from 'src/app/models/frontend/proCart';
import { TablesService } from 'src/app/services/tables.service';
import { identifierName } from '@angular/compiler';
import { NotificationService } from 'src/app/services/notification.service';
import { DialogComponent } from '../components/dialog/dialog.component';
import { Products } from 'src/app/models/backend/products';
import { Carts } from 'src/app/models/backend/carts';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.scss'],
})
export class CarritoComponent implements OnInit {
  list: proCart[] = [];
  listProCart: ProductsCarts[] = [];
  nuevodato: number = 0;
  spiner = false;
  constructor(
    private service: TablesService,
    public dialog: NotificationService,
    public views: MatDialog
  ) {}

  ngOnInit(): void {
    this.spiner = true;
    this.getList();
  }

  getList() {
    this.service.getAllProductCart().subscribe((data: ProductsCarts[]) => {
      this.list = [];
      this.list.length;
      this.listProCart = data;
      this.listProCart.forEach((element: ProductsCarts) => {
        this.service.getOneCart(element.cart_id).subscribe((cart: Carts) => {
          if (cart) {
            if (cart.status === 'pending') {
              this.service
                .getOneProduct(element.product_id)
                .subscribe((data: Products) => {
                  if (data) {
                    this.spiner = false;
                    const product: proCart = {
                      product: data,
                      product_id: element.product_id,
                      cantidad: element.quantity,
                      id_cart: element.cart_id,
                      id_productCart: element.id,
                    };
                    this.list.push(product);
                  }
                });
            } else {
              this.spiner = false;
            }
          }
        });
      });
    });
  }
  view(item: Products) {
    this.views.open(DialogComponent, {
      data: item,
    });
  }

  async edit(item: proCart) {
    const { value: dato } = await Swal.fire({
      title: 'Cambiar cantidad',
      input: 'number',
      inputLabel: 'Ingrese nueva cantidad',
      inputPlaceholder: 'ingrese nueva cantidad',
      showCancelButton: true,
      validationMessage: 'Ingrese Solamente numeros arriba de 1',
    });
    if (dato <= 0) {
      Swal.fire('No se puedo Modificar', 'dato no valido', 'info');
    } else if (dato > 0) {
      const product: ProductsCarts = {
        product_id: item.product_id,
        cart_id: item.id_cart,
        quantity: dato,
        id: item.id_productCart,
      };
      this.service.updataProductCart(item.id_productCart, product).then(() => {
        console.log(product);
        this.dialog.toastSuccess('Se Modifico correctente');
      });
    }
  }

  delate(producCart: proCart, i: any) {
    const newList = this.list.splice(i, 1);
    this.list = newList;
    this.service.delateCart(producCart.id_cart).then(() => {
      this.service.delateProductCart(producCart.id_productCart).then(() => {
        this.dialog.toastSuccess('Se Elimino correctente');
      });
    });
  }

  purchase() {
    let num: number = 0;
    this.list.forEach((element: proCart) => {
      num++;
      this.service.updateCart(element.id_cart).then(() => {});
    });
    if (this.list.length === num) {
      this.service.createOrden(this.list);
      this.list = [];

      this.dialog.toastSuccess('usted compro correctente');
    }
  }
}
