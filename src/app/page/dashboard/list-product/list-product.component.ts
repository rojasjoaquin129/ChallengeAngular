import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs';
import { DataService } from 'src/app/services/data.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertService } from 'src/app/services/alert.service';
import { TablesService } from 'src/app/services/tables.service';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss'],
})
export class ListProductComponent implements OnInit {
  flag = false;
  completo: any;
  quantity: number = 0;
  defaultColDef: any;
  total: number = 0;
  list: any[] = [];
  progress: boolean = false;
  constructor(
    private dataService: DataService,
    private alert: AlertService,
    private service: TablesService
  ) {}

  onchange(event: any) {
    timer(500).subscribe(() => {
      this.flag = false;
    });
    this.completo = event;
    this.flag = true;
  }

  upQuantity() {
    if (this.completo) {
      this.quantity++;
      this.total = this.quantity * this.completo.price;
    } else {
      this.alert.showNotification('no elejio  un producto', 'aceptar', 'error');
    }
  }

  downQuantity() {
    if (this.completo && this.quantity > 0) {
      this.quantity--;

      this.total = this.quantity * this.completo.price;
    } else {
      this.alert.showNotification(
        'No se puede elejir menos de 0',
        'aceptar',
        'error'
      );
    }
  }

  add() {
    this.progress = true;
    if (this.quantity != 0) {
      console.log(this.completo.id);
      this.service
        .createProductCart(this.completo.id, this.quantity)
        .then(() => {
          this.alert.showNotification(
            'se agregio al carrito correctamente',
            'Aceptar',
            'success'
          );
          this.progress = false;
          this.total = 0;
          this.quantity = 0;
          this.dataService.increm$.emit(1);
        });
    } else {
      this.progress = false;
      this.alert.showNotification(
        'No se puede agregar un 0 de productos al changito',
        'Aceptar',
        'error'
      );
    }
  }
  ngOnInit(): void {}
}
