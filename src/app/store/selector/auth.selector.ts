import { createSelector, createFeatureSelector } from '@ngrx/store';
import { AppState } from '../index';
import { UserState } from '../reducer/auth.reducer';

export const creatFeature = (state: AppState) => state.authState;

export const getUser = createSelector(creatFeature, (state) => state.entity);
export const getLoading = createSelector(
  creatFeature,
  (state) => state.loading
);

export const getIsAuthorized = createSelector(
  creatFeature,
  (state) => !!state.uid
);
