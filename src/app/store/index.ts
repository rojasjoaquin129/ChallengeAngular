import { ActionReducerMap } from '@ngrx/store';
import { AuthEffects } from './effects/auth.effects';
import * as auth from './reducer/auth.reducer';
export interface AppState {
  authState: auth.UserState;
}

export const ROOT_REDUCERS: ActionReducerMap<AppState> = {
  authState: auth.AuthReducer,
};
export const effects = [AuthEffects];
