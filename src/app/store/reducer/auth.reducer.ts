import { table } from 'console';

import { createReducer, on } from '@ngrx/store';
import * as actiones from '../actions/auth.actions';
export interface UserState {
  entity: string | null;
  uid: string | null;
  loading: boolean | null;
  error: string | null;
}

const initialState: UserState = {
  entity: null,
  uid: null,
  loading: null,
  error: null,
};

export const AuthReducer = createReducer(
  initialState,
  on(actiones.SignIn, (state) => {
    return { ...state, login: true };
  }),
  on(actiones.SignInSuccess, (state, { token, email }) => {
    return { ...state, entity: email, uid: token, loading: false };
  }),
  on(actiones.SignInError, (state, { error }) => {
    return { ...state, loading: false, error: error };
  }),
  on(actiones.SignUp, (state) => {
    return { ...state, login: true };
  }),
  on(actiones.SignUpSuccess, (state, { email }) => {
    return { ...state, uid: email, loading: false };
  }),
  on(actiones.SignUpError, (state, { error }) => {
    return { ...state, loading: false, error: error };
  }),
  on(actiones.SignOut, (state) => {
    return { ...state, login: true };
  }),
  on(actiones.SignOutSuccess, (state) => {
    return { ...initialState };
  }),
  on(actiones.SignOutError, (state, { error }) => {
    return { ...state, loading: false, error: error };
  })
);
