import { createAction, props } from '@ngrx/store';
import { Auth } from 'src/app/models/frontend/auth.ts/auth';

export enum types {
  signIn = '[AUTH] login con email:start',
  sigInSuccess = '[AUTH] login con email:start',
  sigInError = '[AUTH] login con email:start',

  signUp = '[AUTH] Sign Up con email:start',
  sigUpSuccess = '[AUTH] Sign Up con email:Success ',
  sigUpError = '[AUTH] Sign Up con email:Error',

  signOut = '[AUTH] Sign Out con email:start',
  sigOutSuccess = '[AUTH] Sign Out con email:Success',
  sigOutError = '[AUTH] Sign Out con email: Error',
}

/**
 * lo hago asi porq nose como hacen como redux  eso de implementar la clase action
 * y q cada accions sea una clase
 *
 * */

//login
export const SignIn = createAction(
  types.signIn,
  props<{ email: string; password: string }>()
);

export const SignInSuccess = createAction(
  types.sigInSuccess,
  props<{ token: string; email: string }>()
);

export const SignInError = createAction(
  types.signIn,
  props<{ error: string }>()
);

//registro
export const SignUp = createAction(
  types.signUp,
  props<{ email: string; password: string }>()
);

export const SignUpSuccess = createAction(
  types.sigUpSuccess,
  props<{ email: string }>()
);

export const SignUpError = createAction(
  types.sigUpSuccess,
  props<{ error: string }>()
);

//logout
export const SignOut = createAction(types.signOut);
export const SignOutSuccess = createAction(types.sigOutSuccess);
export const SignOutError = createAction(
  types.sigOutError,
  props<{ error: string }>()
);
