import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { from, of } from 'rxjs';
import {
  catchError,
  exhaustMap,
  map,
  mergeMap,
  switchMap,
  take,
  tap,
} from 'rxjs/operators';

import * as actiones from '../actions/auth.actions';
import { SignUp } from '../actions/auth.actions';
import { AlertService } from 'src/app/services/alert.service';
import { environment } from 'src/environments/environment';

import { AuthService } from 'src/app/services/auth.service';
import { TablesService } from 'src/app/services/tables.service';
import { type } from 'os';
import { Action } from 'rxjs/internal/scheduler/Action';
@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private afAuth: AngularFireAuth,
    private alert: AlertService,
    private auth: AuthService,
    private tables: TablesService
  ) {}

  register$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actiones.SignUp),
      switchMap((action) =>
        from(this.auth.register(action.email, action.password)).pipe(
          tap(async () => {
            this.auth.sendVerificationEmail();
          }),
          map((data) => ({
            type: actiones.types.sigUpSuccess,
            uid: data.user ? data.user.uid : '',
          })),
          catchError((err) => {
            this.alert.showNotification(
              'Error al querer registrarte',
              'ok',
              'error'
            );
            return of({
              type: actiones.types.sigUpError,
              error: err.message,
            });
          })
        )
      )
    )
  );

  registerSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actiones.SignUpSuccess),
        tap((action) => {
          localStorage.setItem('email', action.email);
          this.alert.showNotification(
            'Se Registro perfectamente',
            'ok',
            'success'
          );
          this.router.navigate(['backoffice/Dashboard']);
        })
      ),
    { dispatch: false }
  );
  //-------------- registro fail----------
  registerFail$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actiones.SignOutError),
        tap((action) => {
          this.alert.showNotification(
            'El email ya esta en la base de datos',
            'Ok',
            'error'
          );
        })
      ),
    { dispatch: false }
  );

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actiones.SignIn),
      switchMap((action) =>
        from(this.auth.login(action.email, action.password)).pipe(
          map((result) => ({
            type: actiones.types.sigInSuccess,
            uid: result.user ? result.user.uid : '',
            user: result.user?.emailVerified,
          })),
          catchError((err) => {
            this.alert.showNotification(
              'Error al querer logearte',
              'ok',
              'error'
            );
            return of({
              type: actiones.types.sigInError,
              error: err.message,
            });
          })
        )
      )
    )
  );
  loginSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actiones.SignInSuccess),
        tap((action) => {
          localStorage.setItem('token', action.token);
          if (action.email) {
            this.router.navigate(['dashboard']);
          } else {
            this.alert.showNotification(
              'No verifico su cuenta con su email',
              'Ok',
              'error'
            );
          }
        })
      ),
    { dispatch: false }
  );
  loginFail$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actiones.SignInError),
        tap((action) => {
          this.alert.showNotification(
            'Error en el correo y/o contraseña ',
            'Ok',
            'error'
          );
          this.router.navigate(['public/login']);
        })
      ),
    { dispatch: false }
  );

  logOut$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actiones.SignOut),
      switchMap((action) =>
        from(this.auth.logOut()).pipe(
          map(() => ({
            type: actiones.types.sigOutSuccess,
          })),
          catchError((err) => {
            this.alert.showNotification('Error al querer salir', 'ok', 'error');
            return of({
              type: actiones.types.sigOutError,
              error: err.message,
            });
          })
        )
      )
    )
  );

  logOutSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actiones.SignOutSuccess),
        tap((action) => {
          localStorage.removeItem('token');
          localStorage.removeItem('email');
          this.router.navigate(['/login']);
        })
      ),
    { dispatch: false }
  );

  logOutFail$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actiones.SignInError),
        tap((action) => {
          this.alert.showNotification(
            'Error en el correo y/o contraseña ',
            'Ok',
            'error'
          );
          this.router.navigate(['public/login']);
        })
      ),
    { dispatch: false }
  );
}
